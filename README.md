# stl-to-tif

This is a fork of the stl-to-voxel project by Christian Pedersen:
  https://github.com/rcpedersen/stl-to-voxel

I added *support for writing voxelized meshed to TIF stacks*, using Christoph Gohlke's tifffile.py (http://www.lfd.uci.edu/~gohlke/code/tifffile.py).

Other changes include:
- added option to choose resolution (--resolution or -r)
- added option to set scaling of z dimension (--zscale or -z)
- more verbose command line options and help
- added a bunny.stl example file
- removed padding

Note: 
- requires python 3+

### How to run
```
$ python stltovoxel.py -i ~/path/to/file.stl -o ~/path/to/output.tif -r 100 -z 2.0
```

### Example: 
```
cd stl-to-tif
python stltovoxel.py -i stanford_bunny.stl -o stanford_bunny.tif -r 100 -z 1.0
```

![](stanford_bunny.png "Rendering of TIFF stack created by voxelizing the stanford bunny from STL file")


